package au.edu.unimelb.comp30022.controllertesting;

import android.location.Location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    private TextView costLabel;
    private EditText sourcePostCode;
    private EditText destPostCode;

    @Before
    public void setUp() {
        UI.addWidget("CALCULATE_BUTTON", new Button());
        sourcePostCode = new EditText();
        sourcePostCode.setText("3055");
        UI.addWidget("SOURCE_POST_CODE", sourcePostCode);
        destPostCode = new EditText();
        destPostCode.setText("3010");
        UI.addWidget("DESTINATION_POST_CODE", destPostCode);
        costLabel = new TextView();
        UI.addWidget("COST_LABEL", costLabel);
    }

    @Test
    public void computeDistance() throws Exception {
        AddressTools addressToolsMock = mock(AddressTools.class);
        PostageRateCalculator postageRateCalculator = mock(PostageRateCalculator.class);
        PostcodeValidator postcodeValidator = mock(PostcodeValidator.class);

        Controller controller = new Controller(addressToolsMock, postcodeValidator, postageRateCalculator);

        Location location1 = mock(Location.class);

        when(postcodeValidator.isValid(any(String.class))).thenReturn(true);
        when(postageRateCalculator.computeCost(any(Location.class), any(Location.class))).thenReturn(5);

        controller.calculateButtonPressed();

        assertEquals("$5",costLabel.getText());
    }
}
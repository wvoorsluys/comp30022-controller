package au.edu.unimelb.comp30022.controllertesting;

import android.location.Location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AusParcelPostTest {

    @Before
    public void setUp() {

    }

    @Test
    public void computeDistance900km() throws Exception {

        AusParcelPost parcelPost= new AusParcelPost();

        Location location1 = mock(Location.class);
        Location location2 = mock(Location.class);

        when(location1.distanceTo(location2)).thenReturn(900f);

        assertEquals(5, parcelPost.computeCost(location1, location2));
    }
}